<!DOCTYPE html>
<html>
<head>
	<title>Merdeka!!</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url("asset/css/style.css"); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<img class="image" src="<?= base_url("asset/img/jendral.jpg"); ?>">     
	<p class="content">
		<b class="kutip">"</b>
		<i>Robek-robeklah badanku, potong-potonglah jasad ini, tetapi jiwaku dilindungi benteng merah putih, akan tetap hidup, tetap menuntut bela, siapapun lawan yang aku hadapi.</i><b class="kutip">"</b>
		<br> 
		<p class="author">
			<b><i>Jendral Soedirman - Jogjakarta, 17 Agustus 1948</i></b>
		</p>
	</p>
	<img class="image" src="<?= base_url("asset/img/jendral2.jpg"); ?>"> 
	<p class="content">
		<b class="kutip">"</b>
		<i>
    	Kadang kita terlalu sibuk memikirkan kesulitan-kesulitan sehingga kita tidak punya waktu untuk mensyukuri rahmat Tuhan.</i><b class="kutip">"</b>
		<br> 
		<p class="author">
			<b><i>Jendral Soedirman </i></b>
		</p>
	</p>
	<img class="image" src="<?= base_url("asset/img/jendral3.jpg"); ?>"> 
	<p class="content">
		<b class="kutip">"</b>
		<i>
    	Bahwa satu-satunya hak milik nasional/republic yang masih utuh tidak berubah-ubah, meskipun harus mengalami segala macam soal dan perubahan, hanyalah angkatan perang Republik Indonesia (Tentara Nasional Indonesia)</i><b class="kutip">"</b>
		<br> 
		<p class="author">
			<b><i>Jendral Soedirman - Jogjakarta, 1 Agustus 1949 </i></b>
		</p>
	</p>
</body>
</html>